# NewMarkdownFile

## 介绍
使用python代码实现的一个简单程序，用于在Windows中创建一个目录并存放Markdown文件



## 程序代码

见 `NewMarkdownFile.py`



## 生成可执行程序

要将Python代码保存为可执行程序，你可以使用pyinstaller这个第三方库。PyInstaller可以将Python脚本打包成独立的可执行文件，无需依赖Python解释器。

首先，确保已经安装了PyInstaller库。可以使用以下命令进行安装：

```shell
pip3 install pyinstaller
```

安装完成后，进入保存了Python代码的目录，然后打开终端或命令提示符窗口。

在终端中，运行以下命令来生成可执行文件：

```shell
pyinstaller --onefile NewMarkdownFile.py
```

这个命令会使用PyInstaller将NewMarkdownFile.py文件打包成一个独立的可执行文件（Windows下为文件名.exe，Linux下即为文件名）。生成的可执行文件将位于dist目录下。同时还会生成一个相关的build目录。

完成后，你可以在dist目录下找到生成的可执行文件。你可以将该文件复制到任何地方，并通过双击运行它，而无需安装Python解释器。

注意：生成的可执行文件中可能会包含一些额外的文件和文件夹，这是因为PyInstaller会自动检测代码中所引用的依赖项并将其打包到可执行文件中。你可以根据需要进行清理。



## 使用说明

1. 移动程序到想要创建文档的目录，双击运行刚刚生成的可执行程序`NewMarkdownFile.exe`
2. 根据提示输入文件名，比如：`NewMarkdownFile使用说明`
3. 则程序会在当前目录下，创建一个名为`NewMarkdownFile使用说明`的目录，以及在这个目录下创建一个同名`.md`文件，并且文件的标题也是该名字
4. 输入回车退出程序