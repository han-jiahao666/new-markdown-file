#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# @Time    : 2024/1/9 11:37
# @Author  : Han Jiahao
# @File    : NewMarkdownFile.py
# @Software: PyCharm

import os

def create_dir_and_file(name):
    current_path = os.getcwd()  # 获取当前路径
    dir_path = os.path.join(current_path, name)  # 创建同名目录的路径
    file_path = os.path.join(current_path, name, name + ".md")  # 创建同名文件的路径

    if os.path.exists(dir_path):  # 如果目录已存在，直接返回
        print(f"Directory {name} already exists!")
        return

    os.mkdir(dir_path)  # 创建同名目录
    with open(file_path, "w") as f:  # 在同名目录下创建同名文件
        f.write(f"# {name}")

    print(f"Directory ./{name} and file ./{name}/{name}.md created successfully!")
    input("Press Enter to exit...")

if __name__ == "__main__":
    print("===NewMarkdownFile===")
    name = input("Enter a file name: ")
    create_dir_and_file(name)

